The package has been configured successfully!

Make sure to first define the mapping inside the `contracts/ally.ts` file as follows.

```ts
import { CanvasDriver, CanvasDriverConfig } from 'adonis-ally-canvas/build/standalone'

declare module '@ioc:Adonis/Addons/Ally' {
  interface SocialProviders {
    // ... other mappings
    canvas: {
      config: CanvasDriverConfig
      implementation: CanvasDriver
    }
  }
}
```
