# Adonis Ally Canvas

Adonis Ally Driver for Canvas LMS.

## Getting started

### 1. Install the package

Install the package from your command line.

```bash
npm install --save adonis-ally-canvas
```

or

```bash
yarn add adonis-ally-canvas
```

### 2. Configure the package

```bash
node ace configure adonis-ally-canvas
```

### 3. Validate environment variables

```ts
CANVAS_URL: Env.schema.string(),
CANVAS_CLIENT_ID: Env.schema.string(),
CANVAS_CLIENT_SECRET: Env.schema.string(),
```

### 4. Add variables to your ally configuration

```ts
const allyConfig: AllyConfig = {
  // ... other drivers
  canvas: {
    driver: 'canvas',
    baseUrl: 'https://myaccount.instructure.com'
    clientId: Env.get('CANVAS_CLIENT_ID'),
    clientSecret: Env.get('CANVAS_CLIENT_SECRET'),
    callbackUrl: 'http://localhost:3333/canvas/callback',
  },
}
```
## License

[MIT](LICENSE.md)
