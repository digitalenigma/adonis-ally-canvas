/*
 * Copyright (c) 2022 Joey Chua <jochua@digitalenigma.dev>
 *
 * MIT License.
 */

import type { AllyUserContract, ApiRequestContract } from '@ioc:Adonis/Addons/Ally'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { Oauth2Driver, ApiRequest, RedirectRequest } from '@adonisjs/ally/build/standalone'

/**
 * Canvas access token definition.
 */
export type CanvasDriverAccessToken = {
  token: string
  refreshToken: string
  type: 'bearer'
  user: {
    id: number
    name: string
    global_id: number
    effective_locale: string
  }
}

/**
 * Canvas driver scopes.
 */
export type CanvasDriverScopes = string

/**
 * Canvas configuration definition.
 */
export type CanvasDriverConfig = {
  driver: 'canvas'
  clientId: string
  clientSecret: string
  callbackUrl: string
  baseUrl: string
  authorizeUrl?: string
  accessTokenUrl?: string
  userInfoUrl?: string
}

/**
 * Canvas OAuth2 path.
 */
const CANVAS_PATH_AUTHORIZE = '/login/oauth2/auth'

/**
 * Canvas OAuth2 token path.
 */
const CANVAS_PATH_ACCESS_TOKEN = '/login/oauth2/token'

/**
 * Canvas API v1 User Info.
 */
const CANVAS_PATH_USER_INFO = '/api/v1/users/self'

/**
 * Canvas Driver for AdonisJS Social Authentication.
 */
export class CanvasDriver extends Oauth2Driver<CanvasDriverAccessToken, CanvasDriverScopes> {
  /**
   * The account URL.
   */
  protected baseUrl = ''

  /**
   * The URL for the redirect request.
   */
  protected authorizeUrl = ''

  /**
   * The URL to hit to exchange the authorization code for the access token
   */
  protected accessTokenUrl = ''

  /**
   * The URL to hit to get the user details
   */
  protected userInfoUrl = ''

  /**
   * The param name for the authorization code. Read the documentation of Canvas oauth
   * provider and update the param name to match the query string field name in
   * which the oauth provider sends the authorization_code post redirect.
   */
  protected codeParamName = 'code'

  /**
   * The param name for the error. Read the documentation of Canvas oauth provider and update
   * the param name to match the query string field name in which the oauth provider sends
   * the error post redirect
   */
  protected errorParamName = 'error'

  /**
   * Cookie name for storing the CSRF token. Make sure it is always unique. So a better
   * approach is to prefix the oauth provider name to `oauth_state` value. For example:
   * For example: "facebook_oauth_state"
   */
  protected stateCookieName = 'canvasdriver_oauth_state'

  /**
   * Parameter name to be used for sending and receiving the state from.
   * Read the documentation of Canvas oauth provider and update the param
   * name to match the query string used by the provider for exchanging
   * the state.
   */
  protected stateParamName = 'state'

  /**
   * Parameter name for sending the scopes to the oauth provider.
   */
  protected scopeParamName = 'scope'

  /**
   * The separator indentifier for defining multiple scopes
   */
  protected scopesSeparator = ' '

  constructor(ctx: HttpContextContract, public config: CanvasDriverConfig) {
    super(ctx, config)
    this.authorizeUrl = `${config.baseUrl}${CANVAS_PATH_AUTHORIZE}`
    this.accessTokenUrl = `${config.baseUrl}${CANVAS_PATH_ACCESS_TOKEN}`
    this.userInfoUrl = `${config.baseUrl}${CANVAS_PATH_USER_INFO}`

    /**
     * Extremely important to call the following method to clear the
     * state set by the redirect request.
     *
     * DO NOT REMOVE THE FOLLOWING LINE
     */
    this.loadState()
  }

  /**
   * Configure the authorization redirect request. Canvas
   * requires response_type param to be set.
   */
  protected configureRedirectRequest(request: RedirectRequest<CanvasDriverScopes>) {
    request.param('response_type', 'code')
  }

  /**
   * Optionally configure the access token request. The actual request is made by
   * the base implementation of "Oauth2" driver and this is a hook to pre-configure
   * the request
   */
  // protected configureAccessTokenRequest(request: ApiRequest) {}

  /**
   * Update the implementation to tell if the error received during redirect
   * means "ACCESS DENIED".
   */
  public accessDenied() {
    return this.ctx.request.input('error') === 'user_denied'
  }

  /**
   * Get the user details by query the provider API. This method must return
   * the access token and the user details both.
   */
  public async user(
    callback?: (request: ApiRequest) => void
  ): Promise<AllyUserContract<CanvasDriverAccessToken>> {
    const accessToken = await this.accessToken()
    const user = await this.getUserInfo(accessToken.token, callback)

    return {
      ...user,
      token: accessToken,
    }
  }

  /**
   * Finds the user by the access token.
   */
  public async userFromToken(
    token: string,
    callback?: (request: ApiRequest) => void
  ): Promise<AllyUserContract<{ token: string; type: 'bearer' }>> {
    const user = await this.getUserInfo(token, callback)

    return {
      ...user,
      token: { token, type: 'bearer' as const },
    }
  }

  /**
   * Returns the HTTP request with the authorization header set
   */
  protected getAuthenticatedRequest(url: string, token: string) {
    const request = this.httpClient(url)
    request.header('Authorization', `Bearer ${token}`)
    request.header('Accept', 'application/json')
    request.parseAs('json')
    return request
  }

  /**
   * Fetches the user info from the Canvas API
   */
  protected async getUserInfo(token: string, callback?: (request: ApiRequestContract) => void) {
    const request = this.getAuthenticatedRequest(this.config.userInfoUrl || this.userInfoUrl, token)
    if (typeof callback === 'function') {
      callback(request)
    }

    const body = await request.get()

    return {
      id: body.id,
      nickName: body.name,
      name: body.name,
      email: body.email,
      avatarUrl: body.avatar_url,
      emailVerificationState: 'verified' as const,
      original: body,
    }
  }
}
