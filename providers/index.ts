import type { ApplicationContract } from '@ioc:Adonis/Core/Application'

export default class CanvasProvider {
  constructor(protected app: ApplicationContract) {}

  public async boot() {
    const Ally = this.app.container.resolveBinding('Adonis/Addons/Ally')
    const { CanvasDriver } = await import('../src/Canvas')

    Ally.extend('canvas', (_, __, config, ctx) => {
      return new CanvasDriver(ctx, config)
    })
  }
}
